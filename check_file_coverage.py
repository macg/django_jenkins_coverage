from __future__ import print_function

import json
import sys
import xml.etree.ElementTree as ET

if __name__ == '__main__':
    coverage_file, target_file = sys.argv[1:]
    tree = None

    with open(coverage_file, 'rb') as f:
        tree = ET.parse(f)

    matched_nodes = []
    for node in tree.findall('//class'):
        if target_file.endswith(node.attrib['filename']):
            matched_nodes.append((node, node.attrib['filename']))

    if not matched_nodes:
        sys.exit()

    matched_nodes.sort(key=lambda x: len(x[1]), reverse=True)
    coverage = {}

    for line_entry in matched_nodes[0][0].findall('./lines/line'):
        coverage[line_entry.attrib['number']] = line_entry.attrib['hits']

    print(json.dumps(coverage))
