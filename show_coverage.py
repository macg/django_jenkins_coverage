import json
import sublime
import sublime_plugin
import subprocess
import os


PLUGIN_DIR = os.getcwd()


class ShowCoverageCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        if not hasattr(self.view, 'coverage_shown'):
            self.view.coverage_shown = False

        if self.view.coverage_shown:
            self.view.erase_regions('coverage.covered')
            self.view.erase_regions('coverage.not_covered')
            self.view.coverage_shown = False
            return

        filename = self.view.file_name()
        cur_dir = filename

        folders = set(self.view.window().folders())
        folders.add('/')

        output = None

        while cur_dir not in folders:
            cur_dir = os.path.dirname(cur_dir)
            coverage_file = os.path.join(cur_dir, 'reports/coverage.xml')
            try:
                with open(coverage_file, 'rb'):
                    proc = subprocess.Popen(
                        [
                            'python', 'check_file_coverage.py',
                            coverage_file, filename,
                        ],
                        stdout=subprocess.PIPE,
                        cwd=PLUGIN_DIR,
                    )
                    output = proc.stdout.read()
            except IOError:
                continue
            else:
                break

        if not output:
            return

        try:
            coverage = json.loads(output)
        except ValueError:
            return

        covered, not_covered = [], []
        for line_number, hits in coverage.iteritems():
            line_number, hits = int(line_number), int(hits)
            region = self.view.line(self.view.text_point(line_number - 1, 0))

            if hits == 0:
                not_covered.append(region)
            else:
                covered.append(region)

        self.view.add_regions('coverage.covered', covered, 'markup.inserted', sublime.DRAW_OUTLINED)
        self.view.add_regions('coverage.not_covered', not_covered, 'markup.deleted', sublime.DRAW_OUTLINED)
        self.view.coverage_shown = True
